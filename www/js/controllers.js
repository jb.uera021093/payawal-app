angular.module('starter.controllers', [])

  .controller('AppCtrl', function ($scope, $ionicModal, $state) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.user = user;
    if (user === null) {
      $state.go('login');
    }
  })

  .controller('TransactionsCtrl', function ($scope, $http) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.user = user;
    if (user === null) {
      $state.go('login');
    }
    $scope.transactions = []
    var array = [];
    $scope.getAllTransaction = function () {
      //console.log('Doing login', $scope.loginData);
      var data = $scope.user;
      console.log(data);
      $http({
        method: 'GET',
        url: 'http://payawal.vikings-pawnshop.com/api/v1/mobile/user/' + data.id + '/orders',
        data: data
      }).then(function successCallback(response) {
          var data = response.data.data;
          console.log(data);
          if (response.status == 200) {
            angular.forEach(data, function (val, key) {
              console.log(val.uuid);
              array.push({
                title: val.uuid,
                id: val.uuid,
                created_at: val.created_at,
                paymaya_id: val.paymaya_id,
                reference_code: val.reference_code,
                status: val.status,
                total: val.total
              });
            });
            console.log(array);
            $scope.transactions = array;

            $scope.BarcodeValue = "";
          }
        },
        function errorCallback(response) {
          $scope.showAlert = function () {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'Please enter a correct email and password!'
            });
            alertPopup.then(function (res) {
              console.log('Thank you for not eating my delicious ice cream cone');
            });
          };
        })
    };
    $scope.getAllTransaction();

  })

  .controller('HomeCtrl', function ($scope, $window, $state) {
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    $scope.user = user;
    if (user === null) {
      $state.go('login');
    }
    console.log(user);
    $scope.name = user.details.first_name + " " + user.details.last_name;
    $scope.prefix = "Mr.";
    if (user.details.gender === "Female") {
      $scope.prefix = "Ms.";
    }
  }).controller('TrackingCtrl', function ($scope, $window, $state,$stateParams,$http) {
  var user = JSON.parse(localStorage.getItem('user'));
  console.log(user);
  $scope.user = user;
  if (user === null) {
    $state.go('login');
  }
  $scope.trackings = []
  var array = [];
  $scope.getAllTracking = function () {
    //console.log('Doing login', $scope.loginData);
    var data = $scope.user;
    console.log(data);
    $http({
      method: 'GET',
      url: 'http://payawal.vikings-pawnshop.com/api/v1/mobile/tracking/' + $stateParams.trackingId,
      data: data
    }).then(function successCallback(response) {
        var data = response.data.data;
        console.log(data.records);
        var index = 0;
        for(index;index<data.records.length;index++){
          array.push({
            record:data.records[index],
            date:data.dates[index]
          })
        }
        console.log(array);
        $scope.trackings = array;
        /*if
         (response.status == 200) {
          angular.forEach(data, function (val, key) {
            console.log(val.uuid);
            array.push({
              title: val.uuid,
              id: val.uuid,
              created_at: val.created_at,
              paymaya_id: val.paymaya_id,
              reference_code: val.reference_code,
              status: val.status,
              total: val.total
            });
          });
          console.log(array);


          $scope.BarcodeValue = "";
        }*/
      },
      function errorCallback(response) {
        $scope.showAlert = function () {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Please enter a correct email and password!'
          });
          alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
          });
        };
      })
  };
  $scope.getAllTracking();

}).controller('ProfileCtrl', function ($scope, $window, $http, $state) {
  var user = JSON.parse(localStorage.getItem('user'));
  console.log(user);
  $scope.user = user;
  if (user === null) {
    $state.go('login');
  }
  $scope.updateProfile = function () {
    //console.log('Doing login', $scope.loginData);
    var data = $scope.user;
    console.log(data);
    $http({
      method: 'PUT',
      url: 'http://payawal.vikings-pawnshop.com/api/v1/mobile/user/' + data.id,
      data: data

    }).then(function successCallback(response) {
        //var data = response.data.data;
        console.log(data);
        if (response.status == 200) {
          $window.localStorage.removeItem("user");
          $window.localStorage.setItem("user", JSON.stringify(data));
          $state.go('app.home');
        }
      },
      function errorCallback(response) {
        $scope.showAlert = function () {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Please enter a correct email and password!'
          });
          alertPopup.then(function (res) {
            console.log('Thank you for not eating my delicious ice cream cone');
          });
        };
      })
  };
})
  .controller('TransactionCtrl', function ($scope, $stateParams, $window, $http) {
    var user = JSON.parse(localStorage.getItem('user'));
    $scope.user = user;
    if (user === null) {
      $state.go('login');
    }
    $scope.transaction = null;
    // $scope.customs = [];
    var array = [];
    $scope.getTransactionByUuid = function () {
      //console.log('Doing login', $scope.loginData);
      var data = $scope.user;
      console.log(data);
      $http({
        method: 'GET',
        url: 'http://payawal.vikings-pawnshop.com/api/v1/mobile/user/' + data.id + '/orders/' + $stateParams.transactionId,
        data: data
      }).then(function successCallback(response) {
          var data = response.data.data[0];
          console.log(data);


          if (response.status == 200) {
            $scope.transaction = data;
          }
        },
        function errorCallback(response) {
          $scope.showAlert = function () {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'Please enter a correct email and password!'
            });
            alertPopup.then(function (res) {
              console.log('Thank you for not eating my delicious ice cream cone');
            });
          };
        })
    };
    $scope.getTransactionByUuid();
  })
  .controller('LoginCtrl', function ($scope, $stateParams, $timeout, $http, $ionicPopup, $state, $window) {
    $scope.doLogin = function () {
      //console.log('Doing login', $scope.loginData);
      $http({
        method: 'POST',
        url: 'http://payawal.vikings-pawnshop.com/api/v1/mobile/auth',
        data: {
          email: $scope.loginData.username,
          password: $scope.loginData.password
        },

      }).then(function successCallback(response) {
          var data = response.data.data;
          localStorage.clear();

          console.log(data);
          if (response.status == 200) {
            $window.localStorage.setItem("user", JSON.stringify(data));
            $window.localStorage.setItem("id", data.id);
            $state.go('app.home');
          }
        },
        function errorCallback(response) {
          $scope.showAlert = function () {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'Please enter a correct email and password!'
            });
            alertPopup.then(function (res) {
              console.log('Thank you for not eating my delicious ice cream cone');
            });
          };
        })
    };
  });
